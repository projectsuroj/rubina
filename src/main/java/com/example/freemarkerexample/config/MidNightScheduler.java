package com.example.freemarkerexample.config;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


import java.time.LocalTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @Author MidNightScheduler
 * java-suraj -- 2024-03-30
 */
@Slf4j
@Configuration
@EnableScheduling
public class MidNightScheduler {

    // Scheduled to be executed at the midnight
    // The timezone comes from the application.properties
    // Depending upon the region, timezone can be changed as desired
    @Scheduled(cron = "*/30 * * * * *")
    public void runSchedule() {
        // */10 * * * * *  for midnight

        /*
         * Some code here that mimics actual application logic
         *
         */

        log.info("I am scheduler");

    }

    static {
        log.info("I am static block");
    }

    @PostConstruct
    public void doSomething() {
        log.info("I am post construct");
    }


    public static void main(String[] args) {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

        // Get current time
        LocalTime currentTime = LocalTime.now();

        // Calculate initial delay until 8 PM
        long initialDelay;
        if (currentTime.isBefore(LocalTime.of(20, 0))) {
            initialDelay = currentTime.until(LocalTime.of(7, 0), TimeUnit.HOURS.toChronoUnit());
        } else {
            initialDelay = currentTime.until(LocalTime.of(20, 0).plusHours(1), TimeUnit.HOURS.toChronoUnit());
        }

        // Schedule task to run at 8 PM daily
        scheduler.scheduleAtFixedRate(() -> {
            // Task to be performed at 8 PM
            System.out.println("Task to be performed at 8 PM");
        }, initialDelay, 24, TimeUnit.HOURS);

        // Schedule task to run at 9 PM daily
        scheduler.scheduleAtFixedRate(() -> {
            // Task to be performed at 9 PM
            System.out.println("Task to be performed at 9 PM");
        }, initialDelay + 1, 24, TimeUnit.HOURS);
    }
}

