package com.example.freemarkerexample.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

/**
 * @Author FreeMarkerConfig
 * java-suraj -- 2024-02-26
 */
@Configuration
public class FreeMarkerConfig {
    @Bean(name = "freeMarkerViewResolver")
    public ViewResolver getFreeMakerViewResolver() {
        FreeMarkerViewResolver resolver = new FreeMarkerViewResolver();
        resolver.setPrefix("templates/");
        resolver.setCache(true);
        return resolver;
    }

}
