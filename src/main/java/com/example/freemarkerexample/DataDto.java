package com.example.freemarkerexample;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author DataDto
 * java-suraj -- 2024-02-26
 */

@Data
@NoArgsConstructor
public class DataDto {
    private String DATE;
    private String EVENT_NAME;
    private String EPAY_EVENT_COUNT;
    private String ENS_EVENT_COUNT;
    private String ENS_SUCCESS_PER;
    private String ENS_FAILURE_PER;
    private String VADER_SUCCESS_COUNT;
    private String VADER_FAILURE_COUNT;
    private String VADER_SUCCESS_PER;
    private int VADER_FAILURE_PER;


    public DataDto(String DATE, String EVENT_NAME, String EPAY_EVENT_COUNT, String ENS_EVENT_COUNT, String ENS_SUCCESS_PER, String ENS_FAILURE_PER,
                   String VADER_SUCCESS_COUNT, String VADER_FAILURE_COUNT, String VADER_SUCCESS_PER, int VADER_FAILURE_PER) {
        this.DATE = DATE;
        this.EVENT_NAME = EVENT_NAME;
        this.EPAY_EVENT_COUNT = EPAY_EVENT_COUNT;
        this.ENS_EVENT_COUNT = ENS_EVENT_COUNT;
        this.ENS_SUCCESS_PER = ENS_SUCCESS_PER;
        this.ENS_FAILURE_PER = ENS_FAILURE_PER;
        this.VADER_SUCCESS_COUNT = VADER_SUCCESS_COUNT;
        this.VADER_FAILURE_COUNT = VADER_FAILURE_COUNT;
        this.VADER_SUCCESS_PER = VADER_SUCCESS_PER;
        this.VADER_FAILURE_PER = VADER_FAILURE_PER;
    }

    public static List<DataDto> fetchDummyData() {
        List<DataDto> dataDtoList = new ArrayList<>();
        dataDtoList.add(new DataDto("1", "2", "3", "4", "5", "6", "7", "8", "9", 51));
        dataDtoList.add(new DataDto("1", "2", "3", "4", "5", "6", "7", "8", "9", 15));
        dataDtoList.add(new DataDto("1", "2", "3", "4", "5", "6", "7", "8", "9", 51));
        dataDtoList.add(new DataDto("1", "2", "3", "4", "5", "6", "7", "8", "9", 15));
        return dataDtoList;
    }
}
