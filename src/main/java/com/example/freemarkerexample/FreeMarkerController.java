package com.example.freemarkerexample;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author FreeMarkerController
 * java-suraj -- 2024-02-26
 */
@Slf4j
@RestController
public class FreeMarkerController {

    private final Configuration configuration;

    public FreeMarkerController(Configuration configuration) {
        this.configuration = configuration;
    }

    @GetMapping
    public String emailContentPrinter(Model model) throws TemplateException, IOException {
        Map<String, Object> map = new HashMap<>();
        map.put("dummyData", DataDto.fetchDummyData());
        String html = getEmailContent();
        log.info(html);
        return html;
    }

    String getEmailContent() throws IOException, TemplateException {
        StringWriter stringWriter = new StringWriter();
        Map<String, Object> map = new HashMap<>();
        map.put("dummyData", DataDto.fetchDummyData());
        configuration.getTemplate("email.ftlh").process(map, stringWriter);
        return stringWriter.getBuffer().toString();
    }
}
