package com.example.freemarkerexample.strategypattern.solution;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @Author NotificationProcessor
 * java-suraj -- 2024-03-31
 */
@Component
public class NotificationProcessor {

    private final INotificationService iNotificationService;
    private final NotificationTypeFactory notificationTypeFactory;

    public NotificationProcessor(@Value("${strategy.type}") String notificationType,
                                 NotificationTypeFactory notificationTypeFactory,
                                 ApplicationContext context) {
        this.notificationTypeFactory = notificationTypeFactory;
        this.iNotificationService = notificationTypeFactory.getBean(notificationType, context);
    }


    public void processNotification(String message) {
        iNotificationService.processNotification(message);
    }

}
