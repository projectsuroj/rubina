package com.example.freemarkerexample.strategypattern.solution.impl;

import com.example.freemarkerexample.strategypattern.solution.INotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Author CalsNotificationStrategy
 * java-suraj -- 2024-03-31
 */
@Slf4j
@Component("cals")
public class CalsNotificationStrategy implements INotificationService {

    @Override
    public void processNotification(String message) {
        log.info("Processing via cals");
    }
}
