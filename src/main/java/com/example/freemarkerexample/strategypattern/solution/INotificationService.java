package com.example.freemarkerexample.strategypattern.solution;

public interface INotificationService {
    void processNotification(String message);
}
