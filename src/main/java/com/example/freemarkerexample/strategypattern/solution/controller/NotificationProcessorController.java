package com.example.freemarkerexample.strategypattern.solution.controller;

import com.example.freemarkerexample.strategypattern.solution.NotificationProcessor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author NotificationProcessorController
 * java-suraj -- 2024-03-31
 */

@RestController
@RequestMapping("/strategy-solution")
public class NotificationProcessorController {
    private final NotificationProcessor notificationProcessor;

    public NotificationProcessorController(NotificationProcessor notificationProcessor) {
        this.notificationProcessor = notificationProcessor;
    }

    @GetMapping
    public String solutionMethod() {
        notificationProcessor.processNotification("");
        return null;
    }
}
