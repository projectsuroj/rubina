package com.example.freemarkerexample.strategypattern.solution;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @Author NotificationTypeFactory
 * java-suraj -- 2024-04-01
 */
@Component
public class NotificationTypeFactory {

    public INotificationService getBean(String notificationType, ApplicationContext context) {
        switch (notificationType) {
            case "vadar":
                return (INotificationService) context.getBean("vadar");
            case "cals":
            default:
                return (INotificationService) context.getBean("cals");
        }
    }
}
