package com.example.freemarkerexample.strategypattern.problem;

/**
 * @Author MainProgram
 * java-suraj -- 2024-03-31
 */
public class MainProgram {
    public static void main(String[] args) {
        NotificationProcessor notificationProcessor = new NotificationProcessor();
        notificationProcessor.setNotificationType(NotificationType.CALS);
        notificationProcessor.processNotification("message");
    }
}
