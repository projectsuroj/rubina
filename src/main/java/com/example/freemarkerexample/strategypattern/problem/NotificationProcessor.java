package com.example.freemarkerexample.strategypattern.problem;

import lombok.extern.slf4j.Slf4j;

/**
 * @Author NotificationProcessor
 * java-suraj -- 2024-03-31
 */
@Slf4j
public class NotificationProcessor {
    private NotificationType notificationType;

    public void processNotification(String message) {
        if (notificationType == NotificationType.CALS) {
            log.debug("Processing CALS notification with message => " + message);
        } else if (notificationType == NotificationType.VADAR) {
            log.debug("Processing VADAR notification with message " + message);
        } else {
            throw new IllegalArgumentException("Invalid notification type");
        }
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }
}
