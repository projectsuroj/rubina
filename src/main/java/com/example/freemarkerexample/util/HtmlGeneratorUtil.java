//package com.example.freemarkerexample.util;
//
//    /*
//    The author of this class is java-suraj
//    lightsuraj129@gmail.com
//    Program was written in 2023-05-16 20:29
//    */
//
//import org.springframework.stereotype.Component;
//import org.thymeleaf.TemplateEngine;
//import org.thymeleaf.context.Context;
//import org.thymeleaf.templatemode.TemplateMode;
//import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
//import org.thymeleaf.templateresolver.StringTemplateResolver;
//
//import java.util.Map;
//
//@Component
//public class HtmlGeneratorUtil {
//
//    private static final String UTF_8 = "UTF-8";
//
//    public String createHtml(String templateName, Map<String, Object> map) {
//        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
//        templateResolver.setPrefix("/templates/");
//        templateResolver.setSuffix(".ftl");
//        templateResolver.setTemplateMode("HTML");
//        templateResolver.setTemplateMode(TemplateMode.HTML);
//        templateResolver.setCharacterEncoding(UTF_8);
//
//        TemplateEngine templateEngine = new TemplateEngine();
//        templateEngine.setTemplateResolver(templateResolver);
//
//        Context context = new Context();
//        context.setVariable("data", map);
//        return templateEngine.process(templateName, context);
//    }
//
//    public String processHtmlTemplate(String htmlTemplateString, Map<String, Object> map) {
//        StringTemplateResolver templateResolver = new StringTemplateResolver();
//        templateResolver.setTemplateMode(TemplateMode.HTML);
//
//        TemplateEngine templateEngine = new TemplateEngine();
//        templateEngine.setTemplateResolver(templateResolver);
//
//        Context context = new Context();
//        context.setVariable("data", map);
//
//        String processedHtml = templateEngine.process(htmlTemplateString, context);
//
//        return processedHtml;
//    }
//
//
//    public String replaceSomeText(String text) {
//        text = text.replace("&lt;", "<");
//        text = text.replace("&gt;", ">");
//        text = text.replace("&amp;", "&");
//        text = text.replace("&quot;", "\"");
//        text = text.replace("o:p", "p");
//        return text;
//    }
//}